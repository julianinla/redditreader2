package android.vanrietj.redditreader;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class RedditListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

//        Button button = (Button) view.findViewById(R.id.imageButton);
//        button.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                treehouse = false;
//            }
//        });
//
//        Button button2 = (Button) view.findViewById(R.id.imageButton);
//        button2.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                treehouse = true;
//            }
//        });
//
//        if (treehouse == false) {
//            new RedditPostTask2().execute(this);
//        }
//        else if (treehouse == true) {
//            new RedditPostTask().execute(this);
//        }
        new RedditPostTask().execute(this);

        return view;
    }

    public void updateUserInterface() {
        RedditPostAdapter adapter = new RedditPostAdapter(activityCallback);
        recyclerView.setAdapter(adapter);
    }
}
